#requirements

pytest==7.2.0
pytest-xdist==3.1.0
pytest-cov==4.0.0

click==8.1.3
Flask==2.2.2

# CLI 
PyPdf2

# PySimpleGUI GUI
PySimpleGUI==4.60.4

# PyQt5 GUI
PyQt5==5.15.7

attrdict==2.0.1

# Common testing dependencies
pytest==7.2.0
pytest-mock==3.10.0
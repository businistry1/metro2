from setuptools import setup, find_packages

setup(
    name='metro2_compliance',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        'PyPDF2',
        'PySimpleGUI',
        'regex'
    ],

    entry_points={
        'console_scripts': [
            'metro2_cli=cli:main',
            'metro2_gui_pysimplegui=gui_pysimplegui:main', 
            'metro2_gui=gui:main'
        ],
    }
)

class VerificationSystem:

    def __init__(self, metro2_rules):
        self.metro2_rules = metro2_rules

    def check_compliance(self, credit_report_items):
        # Implement a method to check the compliance of credit report items
        non_compliant_items = []
        for item in credit_report_items:
            # TODO: Check item compliance based on metro2_rules
            if not self.is_item_compliant(item):
                non_compliant_items.append(item)
        return non_compliant_items

    def is_item_compliant(self, item):
        # TODO: Implement a method to determine if an item is compliant
        return True


if __name__ == '__main__':
    metro2_rules = ['The Metro 2']
    verification_system = VerificationSystem(metro2_rules)
    # TODO: Provide credit_report_items for testing
    credit_report_items = []
    non_compliant_items = verification_system.check_compliance(credit_report_items)
    print(non_compliant_items)
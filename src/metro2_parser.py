import PyPDF2
import re


class Metro2Parser:

    def __init__(self):
        pass

    def parse_metro2_rules(self, rules_text):
        # Implement a method to parse and understand Metro2 compliance rules
        rules = re.findall(r'(?<=\n)\d+\.\s+([\w\s]+)', rules_text)
        return rules if rules else ['Parsing issue: Check the rules_text format']

if __name__ == '__main__':
    parser = Metro2Parser()
    # Provide the rules_text from the gathered information
    rules_text = '1. Data furnishers have duties under the Fair Credit Reporting Act (FCRA) to correct and update consumer credit history information.\n2. The Metro 2® Format is a standard electronic data reporting format adopted by the credit reporting industry.'
    parsed_rules = parser.parse_metro2_rules(rules_text)
    print(parsed_rules)

    # Function to extract text from a PDF file
def extract_text_from_pdf(pdf_path):
    text = ""
    with open(pdf_path, "rb") as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        for page in pdf_reader.pages:
            text += page.extract_text()
    return text

# Function to extract compliance rules from text
def extract_compliance_rules(text):
    parser = Metro2Parser()
    rules = parser.parse_metro2_rules(text)
    return rules

# Function to compare rules with credit report information
def compare_compliance_with_credit_report(rules, credit_report_info):
    non_compliant_items = []
    
    for rule in rules:
        if rule['type'] == 'max_credit_limit':
            credit_limits = credit_report_info['credit_limits']
            for limit in credit_limits:
                if limit > rule['limit']:
                    non_compliant_items.append({
                        'rule': rule, 
                        'limit': limit
                    })
        
        elif rule['type'] == 'max_credit_utilization':
            utilizations = credit_report_info['utilizations']
            for utilization in utilizations:
                if utilization > rule['percentage']:
                    non_compliant_items.append({
                        'rule': rule,
                        'utilization': utilization
                    })
            
        # Add cases for other rule types
        
    return non_compliant_items

# Main function
def main():
    pdf_path = "/Metro2 system/src/metro2.pdf"
    credit_report_info = {}  # Placeholder for credit report data

    # Extract text from the PDF
    pdf_text = extract_text_from_pdf(pdf_path)

    # Extract compliance rules from the PDF text
    rules = extract_compliance_rules(pdf_text)

    # Compare compliance rules with credit report information
    out_of_compliance_items = compare_compliance_with_credit_report(rules, credit_report_info)

    # Print results
    print("Items out of compliance:")
    for item in out_of_compliance_items:
        print(item)

if __name__ == "__main__":
    main()

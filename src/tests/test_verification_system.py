from src.verification_system import VerificationSystem

def test_check_compliance():
    rules = ['Rule 1', 'Rule 2']
    system = VerificationSystem(rules)

    # Test compliant item
    item1 = {'name': 'Item 1', 'error': False}
    assert system.check_compliance([item1]) == []
    
    # Test non-compliant item
    item2 = {'name': 'Item 2', 'error': True}
    assert system.check_compliance([item2]) == [item2]
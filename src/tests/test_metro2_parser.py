# Import parser
from src.metro2_parser import Metro2Parser

def test_parse_rules():
    parser = Metro2Parser()
    
    # Test with valid rules
    rules_text = '1. Rule one...\n2. Rule two...'
    expected = ['Rule one', 'Rule two']
    assert parser.parse_metro2_rules(rules_text) == expected

    # Test with invalid format
    invalid_text = 'Not valid rules' 
    assert parser.parse_metro2_rules(invalid_text) == ['Parsing issue: Check the rules_text format']
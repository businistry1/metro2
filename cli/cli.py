from metro2_parser import Metro2Parser
from verification_system import VerificationSystem


def main():
    print('Metro2 Compliance Checker')
    metro2_parser = Metro2Parser()
    # TODO: Provide the 'rules_text' for the 'parse_metro2_rules()' method
    rules_text = '...'
    verification_system = VerificationSystem(metro2_parser.parse_metro2_rules(rules_text))
    print('Check Compliance')
    # TODO: Provide credit_report_items for testing
    non_compliant_items = verification_system.check_compliance([])
    print('Non-compliant items:', non_compliant_items)


if __name__ == '__main__':
    main()
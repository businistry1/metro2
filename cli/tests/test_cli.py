import subprocess

def test_cli():
    output = subprocess.check_output(['python', 'cli/cli.py'])
    assert b'Metro2 Compliance Checker' in output
    assert b'Check Compliance' in output
import tkinter as tk
from metro2_parser import Metro2Parser
from verification_system import VerificationSystem


class Metro2ComplianceGUI:

    def __init__(self, master):
        self.master = master
        self.master.title('Metro2 Compliance Checker')
        # TODO: Create the GUI components and integrate the Metro2 parser and verification system


if __name__ == '__main__':
    root = tk.Tk()
    gui = Metro2ComplianceGUI(root)
    root.mainloop()
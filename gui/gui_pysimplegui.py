import PySimpleGUI as sg
from metro2_parser import Metro2Parser
from verification_system import VerificationSystem


def main():
    # TODO: Create the GUI components and integrate the Metro2 parser and verification system
    layout = [[sg.Text('Metro2 Compliance Checker')],
              [sg.Button('Check Compliance')]]

    window = sg.Window('Metro2 Compliance Checker', layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break
        elif event == 'Check Compliance':
            # TODO: Integrate the Metro2 parser and verification system
            pass

    window.close()


if __name__ == '__main__':
    main()